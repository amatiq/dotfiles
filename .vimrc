syntax on
set mouse=c
set tabstop=2
set autoindent
set expandtab

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SHORTCUTS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = "!" " map leader to comma

map , :tab sball<CR> " Dispatch buffers to tabs
map <c-b> :bdelete<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SYNTAX
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufNewFile,BufRead *.pp set syntax=ruby " Ruby coloration for Puppet files
autocmd BufNewFile,BufRead *.vue set syntax=html " HTML coloration for Vue files
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PLUGIN MANAGER
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf'
Plug 'vim-airline/vim-airline'
Plug 'shime/vim-livedown'
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FZF
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:fzf_preview_window = []
let $FZF_DEFAULT_COMMAND="rg --files --hidden --smart-case --sort path -g '!.git' -g '!.cache'"
map ; :FZF!<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ranger explorer
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function RangerExplorer()
    exec "silent !ranger --choosefile=/tmp/vim_ranger_current_file " . expand("%:p:h")
    if filereadable('/tmp/vim_ranger_current_file')
        exec 'edit ' . system('cat /tmp/vim_ranger_current_file')
        call system('rm /tmp/vim_ranger_current_file')
    endif
    redraw!
endfun
map <Leader>x :call RangerExplorer()<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Livedown 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:livedown_autorun = 0
let g:livedown_open = 1
let g:livedown_port = 1337
let g:livedown_browser = "qutebrowser --target window"

nmap gm :LivedownPreview<CR>
